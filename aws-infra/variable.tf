variable "access_key" {}
variable "secret_key" {}

variable "vpc_cidr_block" {
  description = "CIDR block for VPC"
  type        = string
  default     = "172.16.0.0/16"
}

variable "public_subnets" {
  description = "Public Subnet CIDR"
  type        = string
  default     = "172.16.0.0/24"
}

variable "private_subnets" {
  description = "Private Subnet CIDR"
  type        = string
  default     = "172.16.100.0/24"
}

variable "region" {
  description = "AWS Region"
  type        = string
  default     = "ap-southeast-1"
}

variable "ami_id" {
  description = "AWS AMI ID"
  type        = string
  default     = "ami-0dc5785603ad4ff54"
}

variable "instance_type" {
  description = "Instance type"
  type        = string
  default     = "t2.micro"
}

variable "default_tags" {
  description = "Tags to set for all resources"
  type        = map(string)
  default     = {
    Project = "project-alpha",
    Env     = "dev",
    Owner   = "admin@mail.com"
  }
}

variable "asg_tags" {
  default = [
    {
      key                 = "Project"
      value               = "project-alpha"
      propagate_at_launch = true
    },
    {
      key                 = "Env"
      value               = "dev"
      propagate_at_launch = true
    },
    {
      key                 = "Owner"
      value               = "admin@mail.com"
      propagate_at_launch = true
    }
  ]
}

variable "ingress_rules" {
  type = list(object({
    from        = number
    to          = number
    protocol    = string
    cidr        = list(string)
    description = string
  }))
  default = [{
    cidr = ["172.16.0.0/16"]
    from = 22
    protocol = "tcp"
    to = 22
    description = "Allow SSH from Main VPC"
  },
  {
    cidr = ["0.0.0.0/0"]
    from = 80
    protocol = "tcp"
    to = 80
    description = "Allow HTTP Access from everywhere"
  }]
}
