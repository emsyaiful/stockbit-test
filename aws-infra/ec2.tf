resource "aws_launch_template" "workload-lt" {
  name_prefix   = "workload-lt"
  image_id      = var.ami_id
  instance_type = var.instance_type
  network_interfaces {
    subnet_id = aws_subnet.private.id
    security_groups = [aws_security_group.workload-asg-sg.id]
  }
}

resource "aws_security_group" "workload-asg-sg" {
  name   = "workload-asg-sg" 
  vpc_id = aws_vpc.main-vpc.id
  dynamic "ingress" {
    for_each = var.ingress_rules
    content {
      from_port   = ingress.value.from
      to_port     = ingress.value.to
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr
      description = ingress.value.description
    }
  }

  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
      description = "Allow all traffic to internet"
    }
}

resource "aws_autoscaling_group" "workload-asg" {
  name                      = "workload-asg"
  max_size                  = 5
  min_size                  = 2
  health_check_grace_period = 300
  health_check_type         = "EC2"
  desired_capacity          = 2
  force_delete              = true
  availability_zones        = [aws_subnet.private.availability_zone]
  
  launch_template {
    id = aws_launch_template.workload-lt.id
    version = "$Latest"
  }

  timeouts {
    delete = "5m"
  }

  tags = concat(
    [
      {
        key                 = "Name"
        value               = "workload-asg"
        propagate_at_launch = true
      },
    ],
    var.asg_tags
  )
}

resource "aws_autoscaling_policy" "workload-asg-policy" {
  name                   = "workload-asg-policy"
  policy_type            = "TargetTrackingScaling"
  autoscaling_group_name = aws_autoscaling_group.workload-asg.name

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 45.0
  }
}