resource "aws_vpc" "main-vpc" {
  cidr_block = var.vpc_cidr_block

  enable_dns_support    = true
  enable_dns_hostnames  = true

  tags = merge(
    var.default_tags,
    {
      Name = "${var.default_tags.Project}-vpc"
    }
  )
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main-vpc.id

  tags = merge(
    var.default_tags,
    {
      Name = "${var.default_tags.Project}-igw"
    }
  )
}

resource "aws_subnet" "public" {
  cidr_block = var.public_subnets
  vpc_id     = aws_vpc.main-vpc.id

  map_public_ip_on_launch = true

  tags = merge(
    var.default_tags,
    {
      Name = "${var.default_tags.Project}-public-sn"
    }
  )
}

resource "aws_subnet" "private" {
  cidr_block = var.private_subnets
  vpc_id     = aws_vpc.main-vpc.id

  tags = merge(
    var.default_tags,
    {
      Name = "${var.default_tags.Project}-private-sn"
    }
  )
}

resource "aws_default_route_table" "public" {
  default_route_table_id = aws_vpc.main-vpc.main_route_table_id

  tags = merge(
    var.default_tags,
    {
      Name = "${var.default_tags.Project}-public-rtb"
    }
  )
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_default_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_default_route_table.public.id
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main-vpc.id

  tags = merge(
    var.default_tags,
    {
      Name = "${var.default_tags.Project}-private-rtb"
    }
  )
}

resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private.id
  route_table_id = aws_route_table.private.id
}

resource "aws_eip" "nat-eip" {
  vpc = true

  tags = merge(
    var.default_tags,
    {
      Name = "${var.default_tags.Project}-nat-eip"
    }
  )
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.nat-eip.id
  subnet_id     = aws_subnet.public.id

  tags = merge(
    var.default_tags,
    {
      Name = "${var.default_tags.Project}-nat-gw"
    }
  )
}

resource "aws_route" "private_nat_gateway" {
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat-gw.id
}