terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.70"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region      = var.region
  secret_key  = var.secret_key
  access_key  = var.access_key
}
