vpc_cidr_block  = "10.0.0.0/24"
public_subnets  = "10.0.0.0/25"
private_subnets = "10.0.0.128/25"
region          = "ap-southeast-1"
instance_type   = "t2.medium"
ami_id          = "ami-084009f26f70a7c0b"
secret_key      = "#Place secret key accordingly"
access_key      = "#Place access key accordingly"
ingress_rules = [{
    cidr = ["10.0.0.0/24"]
    from = 22
    protocol = "tcp"
    to = 22
    description = "Allow SSH from Main VPC"
  },
  {
    cidr = ["0.0.0.0/0"]
    from = 80
    protocol = "tcp"
    to = 80
    description = "Allow HTTP Access from everywhere"
  }
]