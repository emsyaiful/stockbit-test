# stockbit-test


## How to use

### Terraform script

1. Fill the AWS secret_key and access_key under the terraform.tfvars (assumption the terrafrom server is outside AWS)
2. Do the clone the aws-infra directory to the terraform server and do the deployment step below
```
$ terraform init
$ terraform plan
$ terraform apply (yes)
``` 
3. Done

### Gitlab CI

1. Build Process
* Build the docker image from the Dockerfile
* Push the docker image to the container repository

2. Deployment Process
* Pull the image from the container repository 
* Run the docker and expose the docker on port 80
